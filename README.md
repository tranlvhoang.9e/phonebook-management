Bài Tập Quản Lý Danh Bạ Điện Thoại
==================================

Bạn đã được giao nhiệm vụ viết phần mềm quản lý danh bạ điện thoại! Dưới đây là những yêu cầu chi tiết để bạn có thể hoàn thành sứ mệnh của mình:

1\. Class Contact
-----------------

Tạo một class có tên là `Contact` với các thuộc tính sau:

-   Tên
-   Số điện thoại
-   Email
-   Nhóm liên lạc (Gia đình, Bạn bè, Công việc) sử dụng enum có tên là `ContactGroup`.
-   Địa chỉ của liên lạc được biểu diễn bằng một đối tượng của class `Address`.

```csharp
class Contact
{
    // Các thuộc tính
    public string Name { get; set; }
    public string PhoneNumber { get; set; }
    public string Email { get; set; }
    public ContactGroup Group { get; set; }
    public Address ContactAddress { get; set; }
}
```

Class `Address` để biểu diễn địa chỉ với các thuộc tính như: số nhà, tên đường, thành phố, quốc gia.

```csharp
class Address
{
    // Các thuộc tính
    public string StreetNumber { get; set; }
    public string StreetName { get; set; }
    public string City { get; set; }
    public string Country { get; set; }
}
```

2\. Class PhoneBook
-------------------

Class `PhoneBook`:

-   Chứa một mảng các đối tượng `Contact`.
-   Cung cấp các chức năng sau:
    -   Thêm mới liên lạc
    -   Xóa liên lạc
    -   Hiển thị danh bạ
    -   Tìm kiếm liên lạc theo tên hoặc số điện thoại
    -   Đọc danh bạ từ file khi chương trình khởi động
    -   Lưu danh bạ xuống file
    -   Thống kê (Tính toán và hiển thị tổng số liên lạc có sẵn trong danh bạ, số lượng danh bạ: Gia đình, Bạn bè, Công việc)

csharpCopy code

```csharp
class PhoneBook
{
    // Các thuộc tính
    public Contact[] contacts;

    // Các phương thức
    // ... (các phương thức đã mô tả trong bài trước)
}
```

3\. Menu Console
----------------

Menu Console để người dùng có thể thao tác với phần mềm:

-   Thêm liên lạc
-   Xóa liên lạc
-   Xem danh sách
-   Tìm kiếm liên lạc (Cho phép người dùng tìm kiếm liên lạc theo tên hoặc số điện thoại)
-   Thống kê
-   Thoát chương trình.

Lưu ý: Đảm bảo kiểm soát và xử lý các trường hợp ngoại lệ có thể xảy ra trong quá trình thao tác với danh bạ điện thoại. Hãy tạo trải nghiệm người dùng thú vị và thuận tiện để họ có thể dễ dàng tương tác với chương trình của bạn. Chúc bạn thành công trong nhiệm vụ của mình!