﻿public class PhoneBookRun
{
    /// <summary>
    /// Hiển thị menu chọn lựa trên Console.
    /// </summary>
    public void DisplayMenu()
    {

        Console.WriteLine("1. Thêm Liên Lạc");
        Console.WriteLine("2. Xóa Liên Lạc");
        Console.WriteLine("3. Hiển Thị Tất cả Liên Lạc");
        Console.WriteLine("4. Tìm Kiếm Liên Lạc");
        Console.WriteLine("5. Hiển Thị Thống Kê");
        Console.WriteLine("6. Thoát");

        Console.Write("Nhập lựa chọn của bạn: ");
    }

    /// <summary>
    /// Chạy chương trình quản lý danh bạ.
    /// </summary>
    public void Run()
    {
        // Khởi tạo đối tượng PhoneBookUI
        PhoneBookUI phoneBookUI = new PhoneBookUI();
        Console.OutputEncoding = System.Text.Encoding.UTF8;
        while (true)
        {
            Console.Clear(); // xóa đi những content cũ trên console
            DisplayMenu(); // Hiển thị menu
            string choice = Console.ReadLine();

            switch (choice)
            {
                case "1":
                    Console.WriteLine("1");
                    phoneBookUI.AddContact();
                    break;

                case "2":
                    Console.WriteLine("2");
                    phoneBookUI.RemoveContact();
                    break;

                case "3":
                    Console.WriteLine("3");
                    phoneBookUI.DisplayContacts();
                    break;

                case "4":
                    Console.WriteLine("4");
                    phoneBookUI.SearchContacts();
                    break;

                case "5":
                    Console.WriteLine("5");
                    phoneBookUI.ShowStatistics();
                    break;

                case "6":
                    Console.WriteLine("6");
                    Console.WriteLine("Thoát chương trình");
                    return;

                default:
                    Console.WriteLine("Lựa chọn không hợp lệ");
                    break;
            }

            Console.WriteLine();
            Console.WriteLine("Bấm phím bất kỳ để tiếp tục ...");
            Console.ReadLine();
        }
    }
}

