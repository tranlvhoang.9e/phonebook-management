﻿public class PhoneBookUI
{
    public PhoneBook phoneBook;

    public PhoneBookUI()
    {
        phoneBook = new PhoneBook(100);
    }

    public void AddContact()
    {
        Contact contact = new Contact();
        Console.WriteLine("Enter contact details:");
        // nhập dữ liệu liên quan đến contact
        phoneBook.AddContact(contact);
    }

    public void RemoveContact()
    {
        string name = "test";
        phoneBook.RemoveContact(name);
    }

    public void DisplayContacts()
    {
        Contact[] contacts = phoneBook.GetContacts();
        // lấy các contacts và hiển thị lên màn hình 
    }

    public void SearchContacts()
    {
        string keyword = "test";
        Contact[] foundContacts = phoneBook.SearchContact(keyword);
        // lấy các foundContacts và hiển thị lên màn hình 
    }

    public void ShowStatistics()
    {
        Statistic statistics = phoneBook.CalulatedStatistics();
        Console.WriteLine($"TotalContact in the phone book: {statistics.TotalContact}");
        Console.WriteLine($"Total Work contacts in the phone book: {statistics.TotalWorkContact}");
        Console.WriteLine($"Total Friend contacts in the phone book: {statistics.TotalFriendContact}");
        Console.WriteLine($"Total Family contacts in the phone book: {statistics.TotalFamilyContact}");
    }
}