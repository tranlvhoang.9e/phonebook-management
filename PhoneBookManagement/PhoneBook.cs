﻿public class PhoneBook
{
    // Các thuộc tính
    public Contact[] contacts;

    /// <summary>
    /// Khởi tạo đối tượng PhoneBook với số lượng contact tối đa được chỉ định.
    /// </summary>
    /// <param name="maximumContact">Số lượng contact tối đa có thể lưu</param>
    public PhoneBook(int maximumContact)
    {
        contacts = new Contact[maximumContact];
        // TODO: Load dữ liệu từ file contacts.json khi khởi tạo đối tượng.
    }

    /// <summary>
    /// Thêm một contact mới vào danh bạ.
    /// </summary>
    /// <param name="contact">Contact cần thêm</param>
    public void AddContact(Contact contact)
    {
        // TODO: Thêm logic để thêm contact vào danh bạ.
        throw new NotImplementedException();
    }

    /// <summary>
    /// Xóa một contact khỏi danh bạ dựa trên tên.
    /// </summary>
    /// <param name="name">Tên của contact cần xóa</param>
    public void RemoveContact(string name)
    {
        // TODO: Thêm logic để xóa contact khỏi danh bạ dựa trên tên.
        throw new NotImplementedException();
    }

    /// <summary>
    /// Lấy danh sách tất cả các contacts trong danh bạ.
    /// </summary>
    /// <returns>Mảng Contact chứa tất cả các contacts</returns>
    public Contact[] GetContacts()
    {
        // TODO: Thêm logic để trả về danh sách tất cả các contacts.
        throw new NotImplementedException();
    }

    /// <summary>
    /// Tìm kiếm contacts trong danh bạ dựa trên từ khóa.
    /// </summary>
    /// <param name="keyword">Từ khóa tìm kiếm (có thể là tên hoặc số điện thoại)</param>
    /// <returns>Mảng Contact chứa các contacts phù hợp với từ khóa</returns>
    public Contact[] SearchContact(string keyword)
    {
        // TODO: Thêm logic để tìm kiếm contacts dựa trên từ khóa.
        throw new NotImplementedException();
    }

    /// <summary>
    /// Lưu danh bạ xuống file.
    /// </summary>
    public void SaveToFile()
    {
        // TODO: Thêm logic để lưu danh bạ xuống file (ví dụ: contacts.json).
        throw new NotImplementedException();
    }

    /// <summary>
    /// Đọc danh bạ từ file khi chương trình khởi động.
    /// </summary>
    public void LoadFromFile()
    {
        // TODO: Thêm logic để đọc danh bạ từ file (ví dụ: contacts.json) khi chương trình khởi động.
        throw new NotImplementedException();
    }

    /// <summary>
    /// Hiển thị thống kê về danh bạ, ví dụ: tổng số lượng contacts.
    /// </summary>
    /// <returns>Đối tượng Statistic chứa thông tin thống kê</returns>
    public Statistic CalulatedStatistics()
    {
        // TODO: Thêm logic để tính toán và hiển thị thống kê.
        throw new NotImplementedException();
    }
}
